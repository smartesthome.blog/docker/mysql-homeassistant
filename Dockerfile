FROM hypriot/rpi-mysql:5.5

ENV MYSQL_DATABASE=homeassistant
ENV MYSQL_USER=homeassistant

CMD ["mysqld"]
